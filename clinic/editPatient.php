<?php 

require 'php/classes/EditPatient.php';


?>


<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Edit Patient</title>
    </head>
    <body class="login">
       
        <?php
        var_dump($_POST);
             if ( isset($_POST) ) {
                $id = $_POST['id'];
                $edit = new EditPatient();
                $patient = $edit->editPatientById($id);
                var_dump($row);
            }
        ?>
        <form action="php/updatePatient.php" method="post" id="editForm" class="editForm" >
            <h1>Edit Patient</h1>
            <input type="hidden" name="id" value="<?php echo $patient[0]['id']?>">
            <label for="name">Name:</label>
            <input type="text" id="name" name="name" value="<?php echo $patient[0]['name']?>">
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" value="<?php echo $patient[0]['email']?>">
            <label for="phone_number">Phone Number:</label>
            <input type="text" id="phone_number" name="phone_number" value="<?php echo $patient[0]['phone_number']?>">
            <label for="address">Address:</label>
            <input type="text" id="address" name="address" value="<?php echo $patient[0]['address']?>">
            <label for="any_known_medical_condition">Any Known Medical Condition:</label>
            <textarea name="any_known_medical_condition" id="any_known_medical_condition"  
            rows="10"><?php echo $patient[0]['any_known_medical_condition']?></textarea>
            <label for="blood_type">Blood Type:</label>
            <input type="text" id="blood_type" name="blood_type" value="<?php echo $patient[0]['blood_type']?>">
            <button class="logInBtn" type="submit">Edit</button>
        </form>
            
       
    <script src="js/data.js"></script>
    </body>
    </html>