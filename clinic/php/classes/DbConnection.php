<?php 

    class DbConnection{
        protected $db ;
        protected $username ;
        protected $password ;
        protected $host;

        protected function connect(){
            $db = 'pabau';
            $username = 'root';
            $password = '';
            $host = 'localhost';
            $conn = new PDO("mysql:host={$host};dbname={$db};","{$username}","{$password}");
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

            return $conn;
        }
    }

?>