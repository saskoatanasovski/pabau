<?php 
    require 'DbConnection.php';
    class UpdatePatient extends DbConnection{
        public $id;
        public $name;
        public $email;
        public $phone_number;
        public $address;
        public $any_known_medical_condition;
        public $blood_type;

        public function __construct($id,$name,$email,$phone_number,$address,$any_known_medical_condition,$blood_type)
        {
            $this->id = $id;
            $this->name = $name;
            $this->email = $email;
            $this->phone_number = $phone_number;
            $this->address = $address;
            $this->any_known_medical_condition == $any_known_medical_condition;
            $this->blood_type = $blood_type;
        }

        public function updatePatientByid(){
            $sql = "UPDATE patient 
            SET 
            name = {$this->name},
            email = {$this->email},
            phone_number = {$this->phone_number},
            address = {$this->address},
            any_known_medical_condition = {$this->any_known_medical_condition},
            blood_type = {$this->blood_type}
            WHERE id = {$this->id}
            ";
            $query = $this->connect()->query($sql);
        }
    }

?>