<?php 

require 'php/classes/PatientList.php';


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Patient List</title>
</head>

<body>
    <div class="container">
        <h1 class="text-center mb-3">Patients List</h1>
        <div class="row">
            <div class="col-md-5 p-0 m-0">
                <form class="form-inline" action="" method="POST">
                    <div class="form-group mx-sm-3 ">
                        <input type="hidden" name="find" value="find">
                        <label for="findPatient" class="mr-2">Find by ID:</label>
                        <input type="text" class="form-control" id="findPatient" name="findPatient">
                    </div>
                    <button type="submit" class="btn btn-primary">Find</button>
                </form>
            </div>
            <div class="col-md-2">
            <form class="form-inline" action="" method="POST">
                    <div class="form-group mx-sm-3 ">
                        <input type="hidden" name="selectAll" value="selectAll">
                    </div>
                    <button type="submit" class="btn btn-primary">Select All</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col  mt-4">
                
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Address</th>
                            <th scope="col">Any Known Medical Condition</th>
                            <th scope="col">Blood Type</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                       
                            if (isset($_POST['find']) && $_POST['find'] == 'find') {
                                $id = $_POST['findPatient'];
                                
                                $patient = new PatientList();
                                foreach ($patient->findPatientById($id) as $key => $list){
                                    echo "<tr>";
                                    echo "<th>{$list['id']}</th>";
                                    echo "<td>{$list['name']}</td>";
                                    echo "<td>{$list['email']}</td>";
                                    echo "<td>{$list['phone_number']}</td>";
                                    echo "<td>{$list['address']}</td>";
                                    echo "<td>{$list['any_known_medical_condition']}</td>";
                                    echo "<td>{$list['blood_type']}</td>";
                                    echo "<td>";
                                    echo    "<button data-id={$list['id']} type=button class='btn btn-primary edit'><i
                                                    class='fa fa-pencil' aria-hidden='true'></i></button>";
                                    echo    "<button data-id='{$list['id']}' type='button'
                                                class='btn btn-danger mt-3 delete'><i class='fa fa-trash-o'
                                                aria-hidden='true'></i></button>";
                                    echo "</td>";
                                    echo "</tr>";
                                        
                                }
                            }
                            else if(isset($_POST['selectAll']) && $_POST['selectAll'] == 'selectAll'){
                                $patient = new PatientList();
                                foreach ($patient->printPatientList() as $key => $list){
                                    echo "<tr>";
                                    echo "<th>{$list['id']}</th>";
                                    echo "<td>{$list['name']}</td>";
                                    echo "<td>{$list['email']}</td>";
                                    echo "<td>{$list['phone_number']}</td>";
                                    echo "<td>{$list['address']}</td>";
                                    echo "<td>{$list['any_known_medical_condition']}</td>";
                                    echo "<td>{$list['blood_type']}</td>";
                                    echo "<td>";
                                    echo    "<button data-id={$list['id']} type=button class='btn btn-primary edit'><i
                                                    class='fa fa-pencil' aria-hidden='true'></i></button>";
                                    echo    "<button data-id='{$list['id']}' type='button'
                                                class='btn btn-danger mt-3 delete'><i class='fa fa-trash-o'aria-hidden='true'></i></button>";
                                    echo "</td>";
                                   echo "</tr>";
                                }
                                clearstatcache();
                            }
                            else{
                                $patient = new PatientList();
                                foreach ($patient->printPatientList() as $key => $list){
                                    echo "<tr>";
                                    echo "<th>{$list['id']}</th>";
                                    echo "<td>{$list['name']}</td>";
                                    echo "<td>{$list['email']}</td>";
                                    echo "<td>{$list['phone_number']}</td>";
                                    echo "<td>{$list['address']}</td>";
                                    echo "<td>{$list['any_known_medical_condition']}</td>";
                                    echo "<td>{$list['blood_type']}</td>";
                                    echo "<td>";
                                    echo    "<button data-id={$list['id']} type=button class='btn btn-primary edit'><i
                                                    class='fa fa-pencil' aria-hidden='true'></i></button>";
                                    echo    "<button data-id='{$list['id']}' type='button'
                                                class='btn btn-danger mt-3 delete'><i class='fa fa-trash-o'aria-hidden='true'></i></button>";
                                    echo "</td>";
                                    echo "</tr>";
                                }  
                            }
                            
                        ?>  
                       
                    </tbody>

            </div>
        </div>
    </div>


    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/data.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
</body>

</html>