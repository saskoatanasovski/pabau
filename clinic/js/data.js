

let editBtn = document.querySelectorAll('.edit');
let deleteBtn = document.querySelectorAll('.delete');
let selectAll = document.querySelector('#selectAll');        
editBtn.forEach(element => {
    element.addEventListener('click',function(e){
        window.location.replace('editPatient.php');
        id = e.target.getAttribute('data-id')
        console.log(id)
        let data = new FormData();
        data.append('id',id);

        fetch('editPatient.php',{
            method: "POST",
            body: data
        })
        .then(function(responce) {
            console.log(responce)
            return responce.json();
        })
        .then(function(data){
            console.log(data);
        })
        .catch(function(err) {
            console.log(err);
        });
    });
});
deleteBtn.forEach(element=>{
    element.addEventListener('click',function(e){
        id = e.target.getAttribute('data-id');
        let data = new FormData();
        data.append('id',id);
        console.log(id)
        fetch('deletePatient.php',{
            method: "POST",
            body: data
        })
        .then(function(responce){
            return responce.json();
        })
        .then(function(data){

        })
        .catch(function(err){
            console.log(err);
        })
        location.reload();
    });
});

