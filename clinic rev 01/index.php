
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Log In</title>
    </head>
    <body class="login">
       
            
        <form action="php/main.php" method="post" class="logInForm" >
            <h1>Log In</h1>
            <?php if (isset($_GET['errorlogin_'])){?>
                <h2 class="text-red"><?php echo $_GET['errorlogin_']?></h2>
            <?php } ?>
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" placeholder="Ex. someone@mail.com">
            <label for="password">Password:</label>
            <input type="password" id="password" name="password">
            <button class="logInBtn" type="submit">Log In</button>
        </form>
            
       
       
    </body>
    </html>