-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2021 at 09:51 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pabau`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(68) DEFAULT NULL,
  `last_name` varchar(68) DEFAULT NULL,
  `email` varchar(68) DEFAULT NULL,
  `password` varchar(68) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'Someone', 'Someone', 'doctor@pabau.com', 'doctor123');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `name` varchar(68) DEFAULT NULL,
  `email` varchar(68) DEFAULT NULL,
  `phone_number` varchar(68) DEFAULT NULL,
  `address` varchar(68) DEFAULT NULL,
  `any_known_medical_condition` text DEFAULT NULL,
  `blood_type` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `name`, `email`, `phone_number`, `address`, `any_known_medical_condition`, `blood_type`) VALUES
(15, 'Florian Gutman', 'borer.clement@example.com', '160-100-9380x44359', '962 Schamberger Burgs Apt. 600\nGutkowskitown, OH 49746', 'Praesentium expedita debitis possimus dolorem iusto et autem debitis. Consequatur delectus optio ipsam porro.', 'minim'),
(16, 'Amos Crist', 'zkirlin@example.com', '(983)732-7827', '04783 Larson Plains Suite 069\nAaliyahfurt, SD 88315', 'In cupiditate qui corporis. Aliquid reiciendis nostrum provident ad aut illum commodi.', 'volup'),
(17, 'Hermina Lemke', 'nathan.barrows@example.com', '648.326.7579x784', '20864 Ward Lock Suite 765\nZulaufhaven, PA 16756', 'Ab est repellendus nesciunt error perferendis. Ullam qui qui corrupti ipsam. Quidem minus voluptatibus et eos voluptatum velit.', 'repel'),
(18, 'Dariana Herzog', 'lehner.marco@example.org', '(149)505-4264x26561', '20752 Idell Road Apt. 316\nCorkeryborough, FL 68156-2506', 'Quo eaque et qui. Est rerum eius possimus aut. In quia expedita est molestias. Voluptatem rerum ut molestiae soluta amet id.', 'volup'),
(19, 'Vilma Reinger', 'trycia.renner@example.org', '1-221-663-8787x2966', '4524 Walsh Forks\nNorth Casimer, AR 60669-4111', 'Est iure dolorem a aut vel aut. Sunt hic est voluptatum labore accusantium. Voluptas quaerat aut autem aut aut. Sed nesciunt et ut voluptas eos repellat ducimus.', 'occae'),
(20, 'Hildegard Dickens DVM', 'haley.runolfsdottir@example.net', '00678890337', '3565 Harley Dam\nOrinmouth, DC 68795', 'Soluta nihil doloribus maiores enim fugiat ullam. Nostrum id qui recusandae voluptatem vero aut. Dicta blanditiis cum assumenda hic et quas. Impedit omnis qui ad doloribus similique.', 'possi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
