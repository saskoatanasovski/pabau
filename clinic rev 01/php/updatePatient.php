<?php

    require 'classes/UpdatePatient.php';

    $id = $_POST['id'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone_number = $_POST['phone_number'];
    $address = $_POST['address'];
    $any_known_medical_condition = $_POST['any_known_medical_condition'];
    $blood_type = $_POST['blood_type'];

    $update = new UpdatePatient($id,$name,$email,$phone_number,$address,$any_known_medical_condition,$blood_type);
    $update->updatePatientByid();

    header('Location: ../patientList.php');
    die;
?>